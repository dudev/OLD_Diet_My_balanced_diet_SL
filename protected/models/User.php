<?php
/**
 * Created by PhpStorm.
 * User: Еввгений
 * Date: 15.07.14
 * Time: 23:37
 *
 * Модель User
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $role
 */
class User extends CActiveRecord {
    const ROLE_ADMIN = 'administrator';
    const ROLE_MODER = 'moderator';
    const ROLE_USER = 'user';
    const ROLE_BANNED = 'banned';

    public function tableName()
    {
        return '{{user}}';
    }

    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->password);
    }

    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
}